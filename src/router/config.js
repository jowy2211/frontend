import Main from '@/components/Main.vue';
import { DefaultLayout } from "@/theme_one/components/layouts";

export const publicRoute = [
  { path: "*", component: () => import(/* webpackChunkName: "errors-404" */ "@/theme_one/views/error/NotFound.vue") },
  {
    path: "/404",
    name: "404",
    meta: { title: "Not Found" },
    component: () => import(/* webpackChunkName: "errors-404" */ "@/theme_one/views/error/NotFound.vue")
  },

  {
    path: "/500",
    name: "500",
    meta: { title: "Server Error" },
    component: () => import(/* webpackChunkName: "errors-500" */ "@/theme_one/views/error/Error.vue")
  }
]

export const protectedRoute = [
	{
		path: '/',
		component: Main
	},
	{
		path: "/HalamanSatu",
		component: DefaultLayout,
		meta: { title: "Home", group: "apps", icon: "" },
		redirect: "/HalamanSatu/dashboard",
		children: [
			{
			path: "/HalamanSatu/dashboard",
			name: "Dashboard",
			meta: { title: "Home", group: "apps", icon: "dashboard" },
			component: () => import(/* webpackChunkName: "dashboard" */ "@/theme_one/views/Dashboard.vue")
			},

			{
			path: "/403",
			name: "Forbidden",
			meta: { title: "Access Denied", hiddenInMenu: true },
			component: () => import(/* webpackChunkName: "error-403" */ "@/theme_one/views/error/Deny.vue")
			}
		]
	},
]